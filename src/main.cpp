extern "C"
{
#include <EGL/egl.h>
#include <GLES3/gl3.h>
#include <GLFW/glfw3.h>
}

#include <iostream>
#include <fstream>
#include <any>
#include <cmath>
#include <thread>

namespace app
{
  GLFWwindow *window;
  
  void process_input ()
  {
    if (glfwGetKey (window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      {
	glfwSetWindowShouldClose (window, true);
      }
  }

  std::string
  load_file (std::string file)
  {
    std::ifstream in (file);
    std::string  file_content;
    if (in.is_open ())
      {   
	while (!in.eof ())
	  {
	    std::string s;
	    getline (in, s);
	    file_content += s + "\n";
	    
	  }
      }
    in.close ();
    return file_content;
  }
}

int
main ()
{
  // Create GLFW window:
  if (!glfwInit ())
    {
      std::cerr << "GLFW failed to initalise!" << std::endl;
      exit (EXIT_FAILURE);
    }

  GLFWmonitor* monitor = glfwGetPrimaryMonitor();
  const GLFWvidmode* mode = glfwGetVideoMode(monitor);
  glfwWindowHint(GLFW_RED_BITS, mode->redBits);
  glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

  app::window = glfwCreateWindow (mode->width, mode->height, PACKAGE, monitor, NULL);
  if (!app::window)
    {
      glfwTerminate ();
      std::cerr << "Failed to create window!" << std::endl;
      exit (EXIT_FAILURE);
    }
  
  glfwMakeContextCurrent(app::window);

  glfwSetInputMode(app::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Triangle:
  glClearColor(0.3f, 0.0f, 0.2f, 1.0f);
  
  float vertices[] =
    {
      -0.5f, -0.5f, 0.0f,
      0.5f, -0.5f, 0.0f,
      0.0f,  0.5f, 0.0f
    };

  // Vertex shader:
  std::string  vertex_shader_src = app::load_file (SHADER_DIR "/shader.vert");
  const char *c_vertex_str = vertex_shader_src.c_str();
  unsigned int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource (vertex_shader, 1, &c_vertex_str, nullptr);
  glCompileShader (vertex_shader);
  int success;
  glGetShaderiv (vertex_shader, GL_COMPILE_STATUS, &success);  
  if (!success)
    {
      char info_log[512];
      glGetShaderInfoLog (vertex_shader, sizeof (info_log), nullptr, info_log);
      std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << info_log << "\n";	  
    }

  // Fragment shader:
  std::string fragment_shader_src = app::load_file (SHADER_DIR "/shader.frag");
  const char *c_fragment_str = fragment_shader_src.c_str ();
  unsigned int fragment_shader = glCreateShader (GL_FRAGMENT_SHADER);
  glShaderSource (fragment_shader, 1, &c_fragment_str, nullptr);
  glCompileShader (fragment_shader);
  glGetShaderiv (fragment_shader, GL_COMPILE_STATUS, &success);
  if (!success)
    {
      char info_log[512];
      glGetShaderInfoLog (vertex_shader, sizeof (info_log), nullptr, info_log);
      std::cout << "ERROR::SHADER:FRAGMENT::COMPILATION_FAILED\n" << info_log << "\n";
    }

  unsigned int shader_program = glCreateProgram ();
  glAttachShader (shader_program, vertex_shader);
  glAttachShader (shader_program, fragment_shader);
  glLinkProgram (shader_program);

  glGetProgramiv (shader_program, GL_LINK_STATUS, &success);
  if (!success)
    {
      char info_log[512];
      glGetProgramInfoLog (shader_program, sizeof (info_log), nullptr, info_log);
    }


  unsigned int vbo, vao;
  glGenBuffers (1, &vbo);
  glBindBuffer (GL_ARRAY_BUFFER, vbo);
  glBufferData (GL_ARRAY_BUFFER, sizeof (vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof (float), static_cast<void *> (0));
  glEnableVertexAttribArray (0);

  float time_value, green_value;
  int vertex_colour_location;

  glBindVertexArray(vao);

  std::jthread input_thread ([&] ()
					 {
					   while (!glfwWindowShouldClose (app::window))
					     {
					       app::process_input ();
					     }
					 });
  
  /* Render loop. */
  while (!glfwWindowShouldClose (app::window))
    { 
      glClear(GL_COLOR_BUFFER_BIT);

      glUseProgram (shader_program);
      time_value = glfwGetTime();
      green_value = (std::sin (time_value) / 2.0f) + 0.5f;
      vertex_colour_location = glGetUniformLocation(shader_program, "ourColour");
      glUniform4f(vertex_colour_location, 0.0f, green_value, 0.0f, 1.0f);
       
      glDrawArrays(GL_TRIANGLES, 0, 3);
      
      glfwSwapBuffers (app::window);
      glfwPollEvents ();
    }

  glfwTerminate ();
  glDeleteShader (vertex_shader);
  glDeleteShader (fragment_shader);
}
